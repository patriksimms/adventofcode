<?php

const ROWS = 127;
const COLS = 7;

$content = file_get_contents('./input.txt');

$positions = explode("\n", $content);
array_pop($positions);

function calcSeatId(string $seat): int
{
    $row = substr($seat, 0, 7);
    $col = substr($seat, 7, 3);

    $rowIndex = findIndexInLine($row, ROWS, "F");
    $colIndex = findIndexInLine($col, COLS, "L");

    return $rowIndex * 8 + $colIndex;
}


function findIndexInLine(string $line, int $upperBorder, string $needle): int
{
    $lowerBorder = 0;
    for ($i = 0; $i < strlen($line); $i++) {
        if ($needle === $line[$i]) {
            $upperBorder = $lowerBorder + intdiv($upperBorder - $lowerBorder, 2);
        } else {
            $lowerBorder = $lowerBorder + intdiv($upperBorder - $lowerBorder, 2) + 1;
        }
    }
    assert($lowerBorder === $upperBorder);
    return $lowerBorder;
}


$hightestSeatId = 0;
$seatIds = [];
foreach ($positions as $position) {
    $seatId = calcSeatId($position);
    $seatIds[$seatId] = $seatId;
    if ($seatId > $hightestSeatId) {
        $hightestSeatId = $seatId;
    }
}
ksort($seatIds);
$currSeatId = 0;
foreach ($seatIds as $seatId) {
    if (!array_key_exists($seatId + 1, $seatIds)) {
        echo $seatId + 1 . "\n";
        break;
    }
}
