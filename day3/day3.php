<?php

$map = file_get_contents('./input.txt');


$map = explode("\n", $map);

function traverseMap(array|bool $map, int $xStepSize, int $yStepSize): int
{
    $x = 0;
    $y = 0;
    $trees = 0;
    while (count($map) - 2 > $y) {
        $x += $xStepSize;
        $x %= strlen($map[0]);
        $y += $yStepSize;
        if ('#' === $map[$y][$x]) {
            $trees++;
        }
    }
    return $trees;
}

$trees = traverseMap($map, 1, 1) *
    traverseMap($map, 3, 1) *
    traverseMap($map, 5, 1) *
    traverseMap($map, 7, 1) *
    traverseMap($map, 1, 2);

echo $trees;
