<?php

include 'validators.php';

use JetBrains\PhpStorm\Pure;

$content = file_get_contents('./input.txt');

$passports = explode("\n\n", $content);

$passports = array_map(function ($passport) {
    $passport = str_replace("\n", " ", $passport);
    $passportParams = explode(" ", $passport);

    $result = [];
    array_walk($passportParams, function (&$value) use (&$result) {
        $splitted = explode(":", $value);
        $result[$splitted[0]] = $splitted[1] ?? '';
    });
    return $result;
}, $passports);


$validPassports = 0;

#[Pure] function arrayKeysExist(array $array, array $keys): bool
{
    return count(array_intersect(array_keys($array), $keys)) == count($keys);
}


foreach ($passports as $passport) {
    $passport = array_filter($passport);
    if (arrayKeysExist($passport, ['byr', 'iyr', 'eyr', 'hgt', 'hcl', 'ecl', 'pid'])) {
        if (validateBirthYear($passport['byr']) &&
            validateIssueYear($passport['iyr']) &&
            validateExpirationDate($passport['eyr']) &&
            validateHeight($passport['hgt']) &&
            validateHairColor($passport['hcl']) &&
            validateEyeColor($passport['ecl']) &&
            validatePassportId($passport['pid'])) {
            $validPassports++;
        }
    }
}

echo $validPassports;
