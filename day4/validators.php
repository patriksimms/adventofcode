<?php

use JetBrains\PhpStorm\Pure;

#[Pure] function validateBirthYear(string $byr): bool
{
    return 4 === strlen($byr) && 1920 <= $byr && 2002 >= $byr;
}

#[Pure] function validateIssueYear(string $iyr): bool
{
    return 4 === strlen($iyr) && 2010 <= $iyr && 2020 >= $iyr;
}

#[Pure] function validateExpirationDate(string $eyr): bool
{
    return 4 === strlen($eyr) && 2010 <= $eyr && 2030 >= $eyr;
}

function validateHeight(string $hgt): bool
{
    if (strpos($hgt, "cm")) {
        $height = explode("cm", $hgt)[0];
        return 150 <= $height && 193 >= $height;
    }
    if (strpos($hgt, "in")) {
        $height = explode("in", $hgt)[0];
        return 59 <= $height && 76 >= $height;
    }
    return false;
}

function validateHairColor(string $hcl): bool
{
    return preg_match('/#[0-9a-f]{6}/', $hcl);
}

#[Pure] function validateEyeColor(string $ecl): bool
{
    $acceptedHairColors = ['amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth'];
    return in_array($ecl, $acceptedHairColors);
}

function validatePassportId(string $pid): bool
{
    return preg_match('/[0-9]{9}/', $pid);
}
