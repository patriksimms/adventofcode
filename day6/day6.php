<?php

$content = file_get_contents('./input.txt');

$groups = explode("\n\n", $content);

$groups = array_map(function ($group) {
    return explode("\n", $group);
}, $groups);

// deleting last element of last group (empty due to \n at eof)
array_pop($groups[count($groups) - 1]);

$correctAnswers = 0;
foreach ($groups as $group) {
    $group = array_map(function ($value) {
        return str_split($value);
    }, $group);
    // call_user_func_array passes ever elem of array to the callback func
    $temp = count(call_user_func_array('array_intersect', $group));
    $correctAnswers += $temp;
}

echo $correctAnswers;
