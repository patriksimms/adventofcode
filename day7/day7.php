<?php

namespace Day7;

require_once('BagStat.php');

$content = file_get_contents('./testinput.txt');

$bagStats = explode(".\n", $content);

array_pop($bagStats);


$bagStats = array_map(function ($bagStat) {
    return new BagStat($bagStat);
}, $bagStats);

for ($i = 0; $i < count($bagStats); $i++) {
    $bagStats[$i]->replaceChildren($bagStats);
}

$count = -1; // -1 becasue shiny cold is, contained in the list itself
foreach ($bagStats as $bag) {
    if ($bag->hasChild('shiny gold')) {
        $count++;
    }
}
echo "task1: " . $count . "\n";


foreach ($bagStats as $bag) {
    if ('shiny gold' === $bag->getBagName()) {
        echo "task2: " . $bag->getChildrenBagCount();
    }
}


//var_dump($bagStats);
