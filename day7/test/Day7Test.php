<?php

namespace Day7\test;

use Day7\BagStat;
use PHPUnit\Framework\TestCase;

class Day7Test extends TestCase
{
    public function testReplaceChildren()
    {
        $content = file_get_contents('./day7/test/testinput.txt');
        $bagStats = explode(".\n", $content);

        $bagStats = array_map(function ($bagStat) {
            return new BagStat($bagStat);
        }, $bagStats);

        for ($i = 0; $i < count($bagStats); $i++) {
            $bagStats[$i]->replaceChildren($bagStats);
        }
        $this->assertEquals(1, 1);
    }

}
