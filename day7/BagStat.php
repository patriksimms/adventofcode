<?php
declare(strict_types=1);

namespace Day7;

class BagStat
{

    private string $bagName;
    private array $rawChildren;
    private array $children;
    private int $amount;

    public function __construct(string $bagStatString)
    {
        $temp = explode(" bags contain ", $bagStatString);
        $this->bagName = $temp[0];
        $this->children = [];
        $this->amount = 1;

        if ('no other bags' !== $temp[1]) {
            $containedBagStats = explode(", ", $temp[1]);
            $this->rawChildren = array_map(function ($bagStat) {
                $bagStat = str_replace("bags", "", $bagStat);
                $bagStat = str_replace("bag", "", $bagStat);
                return ['count' => $bagStat[0], 'bagName' => substr($bagStat, 2)];
            }, $containedBagStats);
        } else {
            $this->rawChildren = [];
        }
    }

    public function hasRawChild(string $bagName): bool
    {
        foreach ($this->rawChildren as $rawChild) {
            if (trim($bagName) === trim($rawChild['bagName'])) {
                return true;
            }
        }
        return false;
    }

    public function addChild(BagStat $bagStat): int
    {
        return array_push($this->children, $bagStat) - 1;
    }

    public function replaceChildren(array $bagStats): void
    {
        if (0 < count($this->rawChildren)) {
            foreach ($bagStats as $bag) {
                if ($this->hasRawChild($bag->bagName)) {
                    $amount = $this->removeRawChild($bag);
                    $index = $this->addChild($bag);
                    $this->children[$index]->setAmount($amount);
                    $this->children[$index]->replaceChildren($bagStats);
                }
            }
        }
    }

    public function removeRawChild(BagStat $bagStat): int
    {
        for ($i = 0; $i < count($this->rawChildren); $i++) {
            if (trim($bagStat->bagName) === trim($this->rawChildren[$i]['bagName'])) {
                $amount = $this->rawChildren[$i]['count'];
                unset($this->rawChildren[$i]);
                $this->rawChildren = array_values($this->rawChildren);
                echo "Removing raw child ...\n";
                return intval($amount);
            }
        }
        // should not reach this
        assert(false);
    }

    public function hasChild(string $childString): bool
    {
        if (trim($this->bagName) === trim($childString)) {
            return true;
        }
        foreach ($this->children as $child) {
            if (trim($child->bagName) === trim($childString)) {
                return true;
            } else {
                if ($child->hasChild($childString)) {
                    return true;
                }
            }
        }
        return false;
    }

    public function getChildrenBagCount(): int
    {
        if (0 === count($this->children)) {
            return $this->amount;
        }
        $childCounts = array_map(function ($child) {
            return $this->amount * $child->getChildrenBagCount();
        }, $this->children);
        return array_sum($childCounts);
    }

    public function setAmount(int $amount): void
    {
        $this->amount = $amount;
    }

    public function getAmount(): int
    {
        return $this->amount;
    }

    public function getBagName(): string
    {
        return $this->bagName;
    }


}
