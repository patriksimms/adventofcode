<?php

$content = file_get_contents('./input.txt');

$lines = explode("\n", $content);

$lines = array_map(function ($line) {
    $splitted = explode(" ", $line);
    $splitted[0] = explode("-", $splitted[0]);
    return $splitted;
}, $lines);

array_pop($lines);

$valid = 0;
foreach ($lines as $line) {
    if ((substr_count($line[2], $line[1][0], intval($line[0][0]) - 1, 1)  === 1) xor
        (substr_count($line[2], $line[1][0], intval($line[0][1]) - 1, 1) === 1)) {
        $valid++;
    }
}

echo "$valid";
